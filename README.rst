=================
Packaging Scripts
=================

make-itp
    Submit an ITP bug for a package that is ready to be uploaded

package-github
    Download source tarball from Github and write packaging files boilerplate

salsa-sync
    Push and pull changes from Salsa. Assumes git-buildpackage packaging style

revert-gbp-import
    Undo an unpublished ``gbp import-orig``

